//
//  ViewController.swift
//  L5_DZ_3_CoffeMachine
//
//  Created by Hen Joy on 4/23/19.
//  Copyright © 2019 Hen Joy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let machine = coffeMachine()
    @IBOutlet weak var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func makeAmericano(_ sender: UIButton) {
        textField.text = machine.makeAmericano()
    }
    
    @IBAction func makeCappucino(_ sender: UIButton) {
        textField.text = machine.makeCapucino()
    }
    
    @IBAction func addWater(_ sender: UIButton) {
        textField.text = machine.fillMachineWater()
    }
    
    @IBAction func addMilk(_ sender: UIButton) {
        textField.text = machine.fillMachineMilk()
    }
    
    @IBAction func addBeans(_ sender: UIButton) {
        textField.text = machine.fillMachineCoffeeBeans()
    }
    
}

