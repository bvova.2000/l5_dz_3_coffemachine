//
//  CoffeMachine.swift
//  L5_DZ_3_CoffeMachine
//
//  Created by Hen Joy on 4/23/19.
//  Copyright © 2019 Hen Joy. All rights reserved.
//

import UIKit

class coffeMachine: NSObject {
    private var water = 0
    private var coffeeBeans = 0
    private var milk = 0
    
    override init() {
        
    }
    init(_water: Int, _coffee: Int, _milk: Int) {
        water = _water
        coffeeBeans = _coffee
        milk = _milk
    }
    override var description: String {
        return "Water in tank = \(water), Coffee beans = \(coffeeBeans), Milk = \(milk)"
    }
    func fillMachineWater() -> String{
        water = 2000
        return "Water in tank = \(water)"
    }
    func fillMachineCoffeeBeans() -> String{
        coffeeBeans = 500
        return "Coffee beans = \(coffeeBeans)"
    }
    func fillMachineMilk() -> String{
        milk = 1000
        return "Milk = \(milk)"
    }
    func makeCapucino() -> String{
        let waterNeed = 300
        let coffeeNeed = 100
        let milkNeed = 200
        if water < waterNeed{
            return "Not enough water"
        }
        if  coffeeBeans < coffeeNeed{
            return "Not enough coffee beans"
        }
        if  milk < milkNeed{
            return "Not enough milk"
        }
        water -= waterNeed
        coffeeBeans -= coffeeNeed
        milk -= milkNeed
        return "Cappucino is ready"
    }
    func makeAmericano() -> String{
        let waterNeed = 150
        let coffeeNeed = 150
        if water < waterNeed{
            return "Not enough water"
        }
        if  coffeeBeans < coffeeNeed{
            return "Not enough coffee beans"
        }
        water -= waterNeed
        coffeeBeans -= coffeeNeed
        return "Americano is ready"
    }
    
}

